<?php

/**
 * Extension Manager/Repository config file for ext "kosmetik_akademie_engel".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Kosmetik Akademie Engel',
    'description' => 'Kosmetik Akademie Engel',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.2.0-10.4.99',
            'fluid_styled_content' => '10.2.0-10.4.99',
            'rte_ckeditor' => '10.2.0-10.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'SchmelzerMedienGmbh\\KosmetikAkademieEngel\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Schmelzer Medien GmbH',
    'author_email' => 'it@schmelzermedien.de',
    'author_company' => 'Schmelzer Medien GmbH',
    'version' => '1.0.0',
];
