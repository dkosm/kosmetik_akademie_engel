/*!
 * Kosmetik Akademie Engel v1.0.0 (https://www.schmelzermedien.de)
 * Copyright 2017-2020 Schmelzer Medien GmbH
 * Licensed under the GPL-2.0-or-later license
 */

// Rellax Settings
var rellax, rellaxB, isMainPage;

// Also can pass in optional settings block
function initRelax () {
    if ((isMainPage && !rellax && $(window).width() >= 1320) ||
        (!isMainPage && !rellax && $(window).width() >= 820)) {
        if ($('.rellax').length) {
            rellax = new Rellax('.rellax', {
                breakpoints: [820, 1020, 1320],
                speed: -2,
                center: true,
                wrapper: null,
                round: true,
                vertical: true,
                horizontal: false
            });
        }
        if ($('.rellax-entry').length) {
            rellaxB = new Rellax('.rellax-entry', {
                breakpoints: [820, 1020, 1320],
                speed: -6,
                center: false,
                wrapper: null,
                round: true,
                vertical: true,
                horizontal: false
            });
        }
    }
}

$(document).ready(function () {
    if ($("#entryHome").html()) {
        isMainPage = true;
    } else {
        isMainPage = false;
    }

    initRelax();

    if ($("#powermail_field_bundleauswahl_01").val() == "") {
        if (window.location.pathname.indexOf("12er") != -1) {
            $("#powermail_field_bundleauswahl_01 option[value='12er-Bundle']").attr("selected", "selected");
            console.log("12");
        } else if (window.location.pathname.indexOf("6er") != -1) {
            $("#powermail_field_bundleauswahl_01 option[value='6er-Bundle']").attr("selected", "selected");
            console.log("6");
        } else if (window.location.pathname.indexOf("kennenlern") != -1) {
            $("#powermail_field_bundleauswahl_01 option[value='Kennenlerntarif']").attr("selected", "selected");
            console.log("lern");
        }
    }
});

// Rellax Destroy
$(window).on("resize", function () {
    if (isMainPage) {
        if (rellax && $(window).width() < 1320) {
            rellax.destroy();
            rellax = null;

            if (rellaxB) {
                rellaxB.destroy();
                rellaxB = null;
            }
        } else if (!rellax && $(window).width() >= 1320) {
            initRelax();
        }
    } else {
        if (rellax && $(window).width() < 820) {
            rellax.destroy();
            rellax = null;

            if (rellaxB) {
                rellaxB.destroy();
                rellaxB = null;
            }
        } else if (!rellax && $(window).width() >= 820) {
            initRelax();
        }
    }
});

// MatchHeight
$(function() {
    $('.additionalinfo').matchHeight({
        target: $('.matchHeight-content')
    });
});

$(function() {
    $('.bundle').matchHeight(
    );
});

// Submenu Open & Close
$(".submenu").click(function() {
    $(this).toggleClass("active-menu");
    $(".submenu-container.main-nav").toggleClass("open-menu");
    $(".submenu-container.hamburger-menu").toggleClass("open-submenu");
});

// Hamburger Menu
$(".hamburger").click(function() {
    $(this).toggleClass("is-active");
    $(".burger-container").toggleClass("open");
});

// Hamburger Menu closes Submenu
$("button.hamburger").on("click", function () {
    closeMobileSubmenu();
});


$(window).on("resize", function () {
    if($(window).width() >= 1020) {
        if ($("button.hamburger").hasClass("is-active")) {
            $(".hamburger").toggleClass("is-active");
            $(".burger-container").toggleClass("open");
            closeMobileSubmenu();
        }
    } else {
        $("ul.mainNav li.nav-item").each(function () {
            if ($(this).hasClass("active-menu")) {
                $(this).click();
            }
        });
    }
});

function closeMobileSubmenu () {
    $(".burger-container .submenu-container").each(function () {
        if ($(this).hasClass("open-submenu")) {
            $(this).parent().click();
        }
    });
}

// Back to Top Button
$('#backToTop').on('click', function (e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: 0
    }, 600, 'easeInExpo');
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 600) { // Wenn 100 Pixel gescrolled wurde
        $('.scrollBackToTop').addClass( "show" );
    } else {
        $('.scrollBackToTop').removeClass( "show" );
    }
});

// Fancybox
$(document).ready(function() {
    $(".fancybox").fancybox();
});
