<?php
defined('TYPO3_MODE') || die();

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'kosmetik_akademie_engel';

    /**
     * Default TypoScript for KosmetikAkademieEngel
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'Kosmetik Akademie Engel'
    );
});
