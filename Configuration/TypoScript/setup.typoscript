######################
#### DEPENDENCIES ####
######################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/setup.typoscript">


################
#### HELPER ####
################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:kosmetik_akademie_engel/Configuration/TypoScript/Helper/DynamicContent.typoscript">


##############
#### PAGE ####
##############
page = PAGE
page {
    typeNum = 0
    shortcutIcon = EXT:kosmetik_akademie_engel/Resources/Public/Icons/favicon.svg

    10 = FLUIDTEMPLATE
    10 {
        # Template names will be generated automatically by converting the applied
        # backend_layout, there is no explicit mapping necessary anymore.
        #
        # BackendLayout Key
        # subnavigation_right_2_columns -> SubnavigationRight2Columns.html
        #
        # Backend Record
        # uid: 1 -> 1.html
        #
        # Database Entry
        # value: -1 -> None.html
        # value: pagets__subnavigation_right_2_columns -> SubnavigationRight2Columns.html
        templateName = TEXT
        templateName {
            cObject = TEXT
            cObject {
                data = pagelayout
                required = 1
                case = uppercamelcase
                split {
                    token = pagets__
                    cObjNum = 1
                    1.current = 1
                }
            }
            ifEmpty = Default
        }
        templateRootPaths {
            0 = EXT:kosmetik_akademie_engel/Resources/Private/Templates/Page/
            1 = {$page.fluidtemplate.templateRootPath}
        }
        partialRootPaths {
            0 = EXT:kosmetik_akademie_engel/Resources/Private/Partials/Page/
            1 = {$page.fluidtemplate.partialRootPath}
        }
        layoutRootPaths {
            0 = EXT:kosmetik_akademie_engel/Resources/Private/Layouts/Page/
            1 = {$page.fluidtemplate.layoutRootPath}
        }

        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                references.fieldName = media
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                levels = 2
                includeSpacer = 1
                as = mainnavigation
            }
        }
    }

    config.noPageTitle = 2
    headerData.1 = TEXT
    headerData.1.field = title
    headerData.1.wrap = <title>| &nbsp;&#x007C; Kosmetik Akademie Engel</title>

    headerData.2 = TEXT
    headerData.2.value(
    <!-- Adobe Fonts -->
    <script type="text/plain" data-cookieconsent="statistics">
      (function(d) {
        var config = {
          kitId: 'qxn3xvj',
          scriptTimeout: 3000,
          async: true
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    </script>
    <!-- End Adobe Fonts -->
    )

    headerData.3 = TEXT
    headerData.3.value(
    <!-- Google Analytics -->
    <script data-ignore="1" data-cookieconsent="statistics" type="text/plain">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-166762698-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    )

    meta {
        viewport = {$page.meta.viewport}
        robots = {$page.meta.robots}
        apple-mobile-web-app-capable = {$page.meta.apple-mobile-web-app-capable}
        description = {$page.meta.description}
        description {
            override.field = description
        }
        author = {$page.meta.author}
        author {
            override.field = author
        }
        keywords = {$page.meta.keywords}
        keywords {
            override.field = keywords
        }
        X-UA-Compatible = {$page.meta.compatible}
        X-UA-Compatible {
            attribute = http-equiv
        }

        # OpenGraph Tags
        og:title {
            attribute = property
            field = title
        }
        og:site_name {
            attribute = property
            data = TSFE:tmpl|setup|sitetitle
        }
        og:description = {$page.meta.description}
        og:description {
            attribute = property
            field = description
        }
        og:image {
            attribute = property
            stdWrap.cObject = FILES
            stdWrap.cObject {
                references {
                    data = levelfield:-1, media, slide
                }
                maxItems = 1
                renderObj = COA
                renderObj {
                    10 = IMG_RESOURCE
                    10 {
                        file {
                            import.data = file:current:uid
                            treatIdAsReference = 1
                            width = 1280c
                            height = 720c
                        }
                        stdWrap {
                            typolink {
                                parameter.data = TSFE:lastImgResourceInfo|3
                                returnLast = url
                                forceAbsoluteUrl = 1
                            }
                        }
                    }
                }
            }
        }
    }

    includeCSSLibs {
        lightbox = EXT:kosmetik_akademie_engel/Resources/Public/Stylesheets/Css/lightbox/lightbox.min.css
    }

    includeCSS {
        fancybox = EXT:kosmetik_akademie_engel/Resources/Public/Stylesheets/Vendors/jquery.fancybox.min.css
        kosmetik_akademie_engel_main = EXT:kosmetik_akademie_engel/Resources/Public/Stylesheets/Css/main.css
    }

    includeJSLibs {

    }

    includeJS {
        jquery = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/jquery-3.5.1.min.js
        jquery-easing = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/jquery.easing.1.3.js
        fancybox2 = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/jquery.fancybox.min.js
    }

    includeJSFooterlibs {

    }

    includeJSFooter {
        matchHeight = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/jquery.matchHeight.min.js
        rellax = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/rellax.min.js
        lightbox = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Libs/lightbox.min.js
        bootstrap = EXT:kosmetik_akademie_engel/Resources/Public/Stylesheets/Vendors/Bootstrap/bootstrap-4.4.1/dist/js/bootstrap.min.js
        kosmetik_akademie_engel_scripts = EXT:kosmetik_akademie_engel/Resources/Public/JavaScript/Dist/scripts.js
    }
}


################
#### CONFIG ####
################
config {
    absRefPrefix = auto
    no_cache = {$config.no_cache}
    uniqueLinkVars = 1
    pageTitleFirst = 1
    linkVars = L
    prefixLocalAnchors = {$config.prefixLocalAnchors}
    renderCharset = utf-8
    metaCharset = utf-8
    doctype = html5
    removeDefaultJS = {$config.removeDefaultJS}
    inlineStyle2TempFile = 1
    admPanel = {$config.admPanel}
    debug = 0
    cache_period = 86400
    sendCacheHeaders = {$config.sendCacheHeaders}
    intTarget =
    extTarget =
    disablePrefixComment = 1
    index_enable = 1
    index_externals = 1
    index_metatags = 1
    headerComment = {$config.headerComment}

    // Disable Image Upscaling
    noScaleUp = 1

    // Compression and Concatenation of CSS and JS Files
    compressJs = 0
    compressCss = 0
    concatenateJs = 0
    concatenateCss = 0
}


################
## POWERMAIL ##
################
plugin.tx_powermail {
    view {
        templateRootPaths.1 = EXT:kosmetik_akademie_engel/Resources/Private/Extensions/powermail/Templates/
        partialRootPaths.1 = EXT:kosmetik_akademie_engel/Resources/Private/Extensions/powermail/Partials/
        layoutRootPaths.1 = EXT:kosmetik_akademie_engel/Resources/Private/Extensions/powermail/Layouts/
    }
}
